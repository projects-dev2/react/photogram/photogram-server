const UserController = require("../controllers/user");

const resolvers = {
    Query: {
        getUser: () => {
            console.log('obteniendo usuario')
            return null;
        }
    },

    Mutation: {
        //User
        register: (_, {input}) => UserController.register(input),
        login: (_, {input}) => UserController.login(input),


    }
}

module.exports = resolvers;