const mongoose = require('mongoose');
const {ApolloServer} = require('apollo-server');
const resolvers = require('./gql/resolver');
const typeDefs = require('./gql/schema');

require('dotenv').config({path: ".env"});

mongoose.connect(process.env.BBDD, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
}, (err, _) => {
    if (err) {
        console.log('Connexion NO establecida');
    } else {
        console.log('Connexion SI establecida');
        server();
    }
});

function server() {

    const serverApollo = new ApolloServer({ typeDefs, resolvers });

    serverApollo.listen().then(({url}) => {
        console.log(`🚀  Server ready at ${url}`);
    })
}
